# R-OpenStreetMap_SurfaceArea, codename OSMR — Surface

R code to calculate and plot items' surface area from OpenStreetMap data

## Roadmap

* Better geo check (Thanks Nominatim…)
* Table output for main indicators
* General theming
* Shiny app for in-browser use